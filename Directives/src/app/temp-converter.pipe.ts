import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempConverter'
})
export class TempConverterPipe implements PipeTransform {

  transform(value: number, unit:string) {
    let res=0;
    if(unit=='c'){
      res=(value-32)*5/9;
    }
    else if(unit=='f'){
      res=(value*9/5)+32;
    }
    return res;
  }

}
