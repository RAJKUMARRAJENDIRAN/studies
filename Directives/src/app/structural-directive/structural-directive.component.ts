import { Component } from '@angular/core';

@Component({
  selector: 'app-structural-directive',
  templateUrl: './structural-directive.component.html',
  styleUrls: ['./structural-directive.component.css']
})
export class StructuralDirectiveComponent {
  
  lowm:number=0;
  avmg:number=0;
  details:any[]=[720858,"Rajkumar R","junior Software Developer","chennai"];
  city:string[]=["chennai", "kochin","puduchery","banglore"];
  selectCity:string="";
}
