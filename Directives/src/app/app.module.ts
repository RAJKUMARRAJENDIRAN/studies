import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StructuralDirectiveComponent } from './structural-directive/structural-directive.component';
import { FormsModule } from '@angular/forms';
import { AttributeDirectiveComponent } from './attribute-directive/attribute-directive.component';
import { TempConverterPipe } from './temp-converter.pipe';
import { PipesComponent } from './pipes/pipes.component';

@NgModule({
  declarations: [
    AppComponent,
    StructuralDirectiveComponent,
    AttributeDirectiveComponent,
    TempConverterPipe,
    PipesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
