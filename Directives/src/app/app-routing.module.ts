import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StructuralDirectiveComponent } from './structural-directive/structural-directive.component';
import { AttributeDirectiveComponent } from './attribute-directive/attribute-directive.component';
import { PipesComponent } from './pipes/pipes.component';

const routes: Routes = [
  {
    path:'structuralDirective',component:StructuralDirectiveComponent
  },
  {path:'',redirectTo:"/structuralDirective",pathMatch:"full"},
  {
    path:'attributedirective',component:AttributeDirectiveComponent
  },{
    path:'pipes',component:PipesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }