import { Component } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent {

  course:string='';

  course1(){
    this.course="Java developing";
  }
  course2(){
    this.course="Web Developing"
  }
  nametxt:string='';
  nametext(event:any){

this.nametxt=(<HTMLInputElement>event.target).value;  }

}
