import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StringInterpolationComponent } from './string-interpolation/string-interpolation.component';
import { PropertyBindingComponent } from './property-binding/property-binding.component';
import { EventBindingComponent } from './event-binding/event-binding.component';
import { TwoWayBindingComponent } from './two-way-binding/two-way-binding.component';
import { TemplateBindingParseResult } from '@angular/compiler';

const routes: Routes = [
  {path:'',redirectTo:'interpolation',pathMatch:'full'},
  {
    path:'interpolation',component:StringInterpolationComponent
  },
  {path:'propertybinding',component:PropertyBindingComponent},
  {
    path:'eventbinding',component:EventBindingComponent
  },
  {
    path:'twowaybinding',component:TwoWayBindingComponent
  },{
    path:'templaterefvar',component:TemplateBindingParseResult
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
