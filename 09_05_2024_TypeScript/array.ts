//  to declare array
let det: any[]=["Rajkumar R", "25-07-1999","neyveli",9524145423];
let fullname:string[]=["RAJKUMAR"," RAJENDIRAN"];
console.log(det.length);
console.log(det);
console.log(fullname);
console.log("FullName(length);",fullname[0].length+fullname[1].length);

//---------------------output---------------------
// C:\TS>tsc array.ts
// C:\TS>node array.js

// 4
// [ 'Rajkumar R', '25-07-1999', 'neyveli', 9524145423 ]
// [ 'RAJKUMAR', ' RAJENDIRAN' ]
// FullName(length); 19

// C:\TS>
