// Parameters in typescript function
// Types of parameter passing
// *Required parameter -used in named functio 
// *optional parameter : may or may not pass values to parameter *it must be followed by " ? ". safe navigation operator.
function add(a, b, c) {
    return a + b;
}
// *default parameter: when we not pass value to parameter it will take the default value passed
function addi(a, b, c) {
    if (c === void 0) { c = 10; }
    return a + b + c;
}
// *rest parameter: it is used to pass multiple values.
//* values are considerd as array form. to pass parameter using "..."
function addition() {
    var a = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        a[_i] = arguments[_i];
    }
    console.log(a);
}
console.log(add(12, 23, 15));
console.log(addi(12.23, 14));
addition(12, 43, 56, 87, 90);
