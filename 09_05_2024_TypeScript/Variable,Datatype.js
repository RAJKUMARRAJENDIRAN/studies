//Typescript Datatpes
// -->predefined Datatype
//     number, string, boolean, any.
// -->UserDefined Datatypes
//     Array, tuple, function, class.
// ** any is a type of datatype which support any type of values.
var dec = 20.5;
var int = 1234;
var bool = true;
var str;
var fullname = "Rajkumar Rajendiran";
console.log(fullname, dec, int, bool);
// to compile 
// tsc filename.ts
// node filename.js
