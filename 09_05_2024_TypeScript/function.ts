// types of function: named function, anonymous function, arrow function

// namedfunction: function created and called by function name 
function add(a:any,b:any)
{
    return a+b;
}
console.log("Named function:",add(124,568));

//--------------------output-------------
// C:\TS>tsc function.ts

// C:\TS>node function.js
// rajkumar

// C:\TS>tsc function.ts

// C:\TS>node function.js
// 692

// C:\TS>

// anonymous function
// to create function without function name. function called without name is called anonymous function. assign the entire logic into an variablename and called by that variable name.

let sub = function(a:any,b:any){
    return a-b;
}
console.log("anonymous Function:",sub(125,64));

//------------------output--------------

// C:\TS>tsc function.ts

// C:\TS>node function.js
// Named function: 692
// anonymous Function: 61

// Arrow function: to create a function without function name and keyword is called arrow function
// to assign entire logic using a fat arrow

let mul=(a:any,b:any)=>{
    return "Arrowfunction "+ (a*b);
}
console.log(mul(12,12));

//-------------------------output--------------------

// C:\TS>tsc function.ts

// C:\TS>node function.js
// Named function: 692
// anonymous Function: 61
// Arrowfunction 144