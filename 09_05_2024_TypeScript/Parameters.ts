// Parameters in typescript function
// Types of parameter passing
    // *Required parameter -used in named functio 
    // *optional parameter : may or may not pass values to parameter *it must be followed by " ? ". safe navigation operator.
    function add(a:number,b:number,c?:number){
        return a+b;
    }
    // *default parameter: when we not pass value to parameter it will take the default value passed
    function addi(a:number,b:number,c:number=10){
        return a+b+c;
    }
    // *rest parameter: it is used to pass multiple values.
    //* values are considerd as array form. to pass parameter using "..."

    function addition(...a:number[]){
        console.log(a);
    }
    console.log(add(12,23,15));
    console.log(addi(12.23,14));
    addition(12,43,56,87,90);

// ------------------output------------------------------------------
//     C:\TS>node Parameters.js
// 35
// 36.230000000000004
// [ 12, 43, 56, 87, 90 ]

// C:\TS>

