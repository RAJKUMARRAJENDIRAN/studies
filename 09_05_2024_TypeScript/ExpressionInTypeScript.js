function degToFar(deg) {
    var f = (deg * 9 / 5) + 32;
    return deg + "c is " + f + "F";
}
function farToDeg(far) {
    var c = (far - 32) * 5 / 9;
    return far + "F  is " + c + "C";
}
console.log(degToFar(32));
console.log(farToDeg(97));
