//Typescript Datatpes
    // -->predefined Datatype
    //     number, string, boolean, any.
    // -->UserDefined Datatypes
    //     Array, tuple, function, class.
    
    // ** any is a type of datatype which support any type of values.
let dec:number=20.5;
let int:number=1234;
let bool:boolean=true;
let str:string;
let fullname:string="Rajkumar Rajendiran";
console.log(fullname, dec, int, bool);

// to compile 
// tsc filename.ts
// node filename.js
// ---------------------output-----------------
// Microsoft Windows [Version 10.0.22631.3447]
// (c) Microsoft Corporation. All rights reserved.

// C:\TS>tsc Variable,Datatype.ts

// C:\TS>node Variable,Datatype.js
// Rajkumar Rajendiran 20.5 1234 true
// C:\TS>