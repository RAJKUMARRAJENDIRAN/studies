// It is a typed array which stores fixed size of values with fixed type.

let details:[string,number,string,any]=["Rajkumar R",24,"chennai",25_07_1999];
console.log(details);

// -----------------------output---------------------------

// C:\TS>tsc Tuple.ts

// C:\TS>node Tuple.js
// [ 'Rajkumar R', 24, 'chennai', 25071999 ]

// C:\TS>